var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('site/index.ejs');
});

router.get('/about', function(req, res, next) {
  res.render('site/about.ejs');
});

router.get('/services', function(req, res, next) {
  res.render('site/services.ejs');
});

router.get('/projects', function(req, res, next) {
  res.render('site/projects.ejs');
});

router.get('/contact', function(req, res, next) {
  res.render('site/contact.ejs');
});


module.exports = router;
